'''
    This program solves word searches of any size, in all 8 possible directions.
    It prints all found words, in the order specified, including their starting
    and ending coordinates, to the user's console.

    It was written for python 3.8, and has also been tested to function on 
    python 3.6

    There are no dependencies other than python itself.

    This program does not work as required. The algorithm I developed for
    diagonal words does not properly work. It looks for the ending character
    of the word, but if it encounters that character somewhere else in the matrix before
    the actual endoing character, it returns that coordinate instead of the correct 
    coordinate of the ending character. I am still working on fixing this right now 
    and will continue working on it even after my submission. 

    Author: Ryan Bandler
'''


import re

#start by opening the file
filePath = str(input("Enter file path: "))  #path to file
f = open(filePath, 'r')                     #temporary file object


data = ""                                   #data in string format
matrixColumns = 0                           #number of columns
matrixRows = 0                              #number of rows
matrix = []                                 #matrix
words = []                                  #list of target words
foundWords = {}                             #dictionary, to ensure correct print order

#add file contents to data string
for line in f:
    data += line

#close temporary file object
f.close()

#convert string into an array of strings containing each line of the data string
#the idea here is to use the first index of the array which contains the array size
#to determine how many following objects should be added to the matrix.
data = data.split('\n')

#parse the first line of our data to detemine the size of our matrix
matrixRows = int(data[0].split('x')[0])
matrixColumns = int(data[0].split('x')[1])

#current index in our data list
index = 1

#add data to the matrix
while index <= matrixRows:
    #split current matrix data into an array of chars
    matrix.append(data[index].split())
    index += 1  #increment index

#add remaining data to the list of target words
for i in range(index, len(data)-1):
    words.append(data[i])
    foundWords[data[i]] = ""

#class for keeping track of found words. Tidy things up a bit. 
class Found(object):

    def __init__(self, word, start, end):
        self.word = word
        self.start = start
        self.end = end
    
    def display(self):
        startStr = ":".join([str(self.start[0]), str(self.start[1])])
        endStr = ":".join([str(self.end[0]), str(self.end[1])])
        print(self.word, startStr, endStr)

#find if word is in a given row. return indices if found, else return None type
def searchHorizontalHelper(arr, target):
    #regex expression to search for word in row
    found = re.search("%s" % (target), "".join(arr))

    #regex expression searching for word reversed in row
    revFound = re.search("%s" % (target[::-1]), "".join(arr))

    #if word is found return starting and ending indices as tuple
    if found:
        return (found.start(), found.end()-1)

    #else if reversed word is found, return its indices reversed
    elif revFound:
        return (revFound.end()-1, revFound.start())

    #else just return None type
    else:
        return None

#applies searchHorizontalHelper to each row in matrix
#prints out found words in given format
def searchHorizontal(mtrx, targets, dictionary):
    #for each row in matrix
    for i in range(len(mtrx)):
        #go through every word in our list of target words
        for target in targets:

            #if word is not found this will be None type
            found = searchHorizontalHelper(mtrx[i], target)
            
            #if found is not None type, add new Found object to dictionary 
            if found:
                dictionary[target] = Found(target, [i, found[0]], [i,found[1]])

#transpose matrix
def transpose(mtrx): 
    mtrx2 = []
    for i in range(len(mtrx[0])):
            row = []
            for j in mtrx:
                row.append(j[i])
            mtrx2.append(row)
    return mtrx2
                

#searching vertical is performed by transposing the matrix and then 
#performing horizontal search on transposed matrix
def searchVertical(mtrx, targets, dictionary):
    #transpose matrix
    mtrx = transpose(mtrx)

    #go through every row in transposed matrix
    for i in range(len(mtrx)):
        #go through every word in the list of target words
        for target in targets:
            
            found = searchHorizontalHelper(mtrx[i], target)

            #if found is not None type, create new Found object and add to dict
            if found:
                dictionary[target] = Found(target, [found[0], i], [found[1], i])

'''
    This algorithm works by starting in the top left corner.
    It moves to the right in the current row until the starting
    character of the target word is found. it saves that coordinate.
    It then moves down and to the right while the current index
    is still matching the word, and it does this until the ending
    character is found. It then returns the starting and ending coordinates.
    If the starting character is not found in a row, it moves to the next row
    and re-starts the process. If the current index moves outside of the matrix
    bounds, The word has not been found and None type is returned.
'''
def searchDiagonalRightDown(mtrx, word):
    i = 0 
    j = 0
    k = 0
    found = False
    start = []
    end = []
    
    try: #if it throws an error, we know we're outside of matrix bounds, meaning word was not found
        while found is False:
            if mtrx[i][j] == word[k]:
                start = [i, j]
                while mtrx[i][j] == word[k] and k < len(word)-1:
                    i += 1
                    j += 1
                    if k+1 < len(word)-1:
                        k += 1
                    wordLastIndex = len(word)-1
                    if mtrx[i][j] == word[wordLastIndex]:
                        end = [i, j]
                        found = True 
                j = 0
                k = 0
            else:
                j += 1
                if j == len(mtrx[0]):
                    j = 0
                    i += 1
        if found:
            return (start, end)
        else:
            return None
    except:
        return None

#same idea idea searchDiagonalRightDown, but starting in the top right
#and working down and to the left
def searchDiagonalLeftDown(mtrx, word):
    i = 0
    j = len(mtrx)-1
    k = 0
    found = False
    start = []
    end = []

    try: #if it throws an error, we know we're outside of matrix bounds, meaning word was not found
        while found is False:
            if mtrx[i][j] == word[k]:
                start = [i, j]
                while mtrx[i][j] == word[k] and k < len(word)-1:
                    i += 1
                    j -= 1
                    if k+1 < len(word)-1:
                        k += 1
                    wordLastIndex = len(word)-1
                    if mtrx[i][j] == word[wordLastIndex]:
                        end = [i, j]
                        found = True 
                j = len(mtrx[0])-1
                k = 0
            else:
                j -= 1
                if j == 0:
                    j = len(mtrx[0])-1
                    i += 1
        if found:
            return (start, end)
        else:
            return None
    except:
        return None

#same idea idea searchDiagonalRightDown, but starting in the bottom left
#and working up and to the right
def searchDiagonalRightUp(mtrx, word):
    i = len(mtrx)-1  
    j = 0 
    k = 0
    found = False
    start = []
    end = []
    
    try: #if it throws an error, we know we're outside of matrix bounds, meaning word was not found
        while found is False:
            if mtrx[i][j] == word[k]:
                start = [i, j]
                while mtrx[i][j] == word[k] and k < len(word)-1:
                    if i-1 >= 0:
                        i -= 1
                    else:
                        return None
                    j += 1
                    if k+1 < len(word)-1:
                        k += 1
                    wordLastIndex = len(word)-1
                    if mtrx[i][j] == word[wordLastIndex]:
                        end = [i, j]
                        found = True 
                j = 0
                k = 0
            else:
                j += 1
                if j == len(mtrx[0])-1:
                    j = 0           
                    i -= 1
        if found:
            return (start, end)
        else:
            return None
    except:
        return None

#same idea idea searchDiagonalRightDown, but starting in the bottom right
#and working up and to the left
def searchDiagonalLeftUp(mtrx, word):
    i = len(mtrx)-1  
    j = len(mtrx[0])-1 
    k = 0
    found = False
    start = []
    end = []
    
    try: #if it throws an error, we know we're outside of matrix bounds, meaning word was not found
        while found is False:
            if mtrx[i][j] == word[k]:
                start = [i, j]
                while mtrx[i][j] == word[k] and k < len(word)-1:
                    if i-1 >= 0:
                        i -= 1
                    else:
                        return None
                    j -= 1
                    if k+1 < len(word)-1:
                        k += 1
                    wordLastIndex = len(word)-1
                    if mtrx[i][j] == word[wordLastIndex]:
                        end = [i, j]
                        found = True 
                j = len(mtrx[0])-1
                k = 0
            else:
                j -= 1
                if j == 0:
                    j = len(mtrx[0])-1           
                    i -= 1
        if found:
            return (start, end)
        else:
            return None
    except:
        return None

# goes through all the search terms and applies the diagonal algorithms to 
# the matrix for each of them. adds them to dictionary if Found
def searchDiagonal(mtrx, targets, dictionary):
    for target in targets:
        foundRD = searchDiagonalRightDown(mtrx, target)
        foundLD = searchDiagonalLeftDown(mtrx, target)
        foundRU = searchDiagonalRightUp(mtrx, target)
        foundLU = searchDiagonalLeftUp(mtrx, target)

        if foundRD:
             dictionary[target] = Found(target, foundRD[0], foundRD[1])
        if foundLD:
             dictionary[target] = Found(target, foundLD[0], foundLD[1])
        if foundRU:
             dictionary[target] = Found(target, foundRU[0], foundRU[1])
        if foundLU:
            dictionary[target] = Found(target, foundLU[0], foundLU[1])

#prints the Found objects in the dictionary
def printResults(dictionary):
    for item in dictionary.items():
        if isinstance(item[1], Found):
            item[1].display()

#searches matrix for all words and prints out indices if found
def wordSearchSolver(mtrx, targets, dictionary):
    searchHorizontal(mtrx, targets, dictionary)
    searchVertical(mtrx, targets, dictionary)
    searchDiagonal(mtrx, targets, dictionary)
    printResults(dictionary)

wordSearchSolver(matrix, words, foundWords)
